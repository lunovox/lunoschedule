
lunoschedule.listSchedules = { }

local myDescription = core.colorize("#00FF00", 
	lunoschedule.translate("SCHEDULE")
).."\n\t* "..lunoschedule.translate("Show the current date.")

for myMonth = 1, 4, 1 do
	for myDay = 1, 28, 1 do
		local ItemID = lunoschedule.getObjectID(myMonth, myDay)
		lunoschedule.listSchedules[#lunoschedule.listSchedules + 1] = ItemID
		local groupsID = lunoschedule.getGroupsID(myMonth, myDay)
		local tileID = lunoschedule.getTileID(myMonth, myDay)
		minetest.register_node(ItemID, {
			description = myDescription,
			inventory_image = tileID,
			tiles = {tileID},
			--inventory_image = "tex_calendar."..lunoschedule.getLanguage()..".png",
			drawtype = "signlike",
			sunlight_propagates = true,
			--paramtype = 'light',
			--light_source = default.LIGHT_MAX, --LIGHT_MAX=14, MaximoReal=15=luz solar
			paramtype2 = "wallmounted",
			walkable = false,
			selection_box = {
				type = "wallmounted",
			},
			is_ground_content = true, --Nao tenho certeza: Se prega no chao?
			groups = groupsID,
			--sounds = default.node_sound_wood_defaults(), --default.node_sound_glass_defaults(),
			sounds = {
				place = "default_place_node_hard",
				default = default.node_sound_wood_defaults(),
			},
			drop = 'lunoschedule:schedule_1_1',
			on_punch = function(pos, node, puncher, pointed_thing)
				lunoschedule.doUpdate(pos)
			end,
			after_place_node = function(pos, placer, itemstack)
				lunoschedule.doUpdate(pos)
			end,
			--Better to use "timer" than "abm" on unnatural nodes (which were not created by mapgen) to alleviate processing.
			on_construct = function(pos)
				minetest.get_node_timer(pos):start(5) -- in seconds
			end,
			on_timer = function(pos)
				lunoschedule.doCheckUpdate(pos)
				return true --true = If will reoeat
			end,
		})
	end
end



minetest.register_craft({
	output = 'lunoschedule:schedule_1_1',
	recipe = {
		{"default:paper"	,"default:paper"			,"default:paper"},
		{"default:paper"	,"default:mese_crystal"		,"default:paper"},
		{"default:paper"	,"default:paper"			,"default:paper"},
	}
})

minetest.register_alias("schedule"		,"lunoschedule:schedule_1_1")
minetest.register_alias(
	lunoschedule.translate("schedule")	
	,"lunoschedule:schedule_1_1"
)

