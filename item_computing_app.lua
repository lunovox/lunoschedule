

if core.global_exists("modComputing") then
   lunoschedule.showFormSchedule = function(player)
      local playername = player:get_player_name()
      local formspec = modComputing.smartphone.getFormBackground()
      local nowMonth = lunoschedule.getNowMonth()
      local nowDay = lunoschedule.getNowDay()
      local tileID = lunoschedule.getTileID(nowMonth, nowDay)
      
      formspec = formspec
      .."image[0.2,2;5,5.0;"..core.formspec_escape(tileID)..";true]"
      .."button[1.2,5.5;3,0.5;btnBackToPhone;"..core.formspec_escape(lunoschedule.translate("BACK")).."]"
      
      core.show_formspec(playername, "frmCalendar", formspec)
   end
   
   core.register_on_player_receive_fields(function(player, formname, fields)
      if type(formname)=="string" and formname=="frmCalendar" then
	      if type(fields)=="table" and type(fields.btnBackToPhone)=="string" then
	         local playername = player:get_player_name()
	         core.show_formspec(playername, "frmSmartphone", modComputing.smartphone.getFormSpec(player))
	         return true --If function returns true, remaining functions are not called
	      end
	   end
   end)
   
   modComputing.add_app("lunoschedule:btnOpenSchedule", {
   	icon_name = "btnOpenSchedule",
   	icon_title = lunoschedule.translate("SCHEDULE"),
   	icon_descryption = lunoschedule.translate("Displays a calendar with the current date."),
   	icon_type = "button", --types: button/button_exit
   	--icon_image = "img_calendar.png",
   	icon_image = "icon_calendar.png",
   	on_iconclick = function(player)
   	   local playername = player:get_player_name()
   	   
   	   core.log(
			   "action","[LUNOSCHEDULE] "
			   ..lunoschedule.translate(
               "Player '@1' is trying to open the schedule via the '@2'!"
               , playername
               , "computing app"
            )
         )
         lunoschedule.showFormSchedule(player)
   	end,
   })
end