lunoschedule = { }

lunoschedule.getNowDaysServer = function()
	return minetest.get_day_count()
end

lunoschedule.getTotalMonths = function()
	local mineDays = lunoschedule.getNowDaysServer()
	return mineDays/28
end

lunoschedule.getTotalYears = function()
	local mineDays = lunoschedule.getNowDaysServer()
	return mineDays/(28*4)
end

lunoschedule.getNowMonth = function() --return 1 to 4
	--math.ceil(arredonda para cima de 0.5), math.floor(arredonda para menor de 0.5)
	local mineDays = lunoschedule.getNowDaysServer()
	--local dayofyear = mineDays%(28*4) 
	return (math.floor(mineDays/28)%4)+1
end

lunoschedule.getNowDay = function() --returne 1 to 28
	--math.ceil(arredonda para cima de 0.5), math.floor(arredonda para menor de 0.5)
	local mineDays = lunoschedule.getNowDaysServer()
	return math.floor(mineDays%28)+1
end
--Date Marking
lunoschedule.getMarkingRow = function(mineDay) --return 1 to 4
	return math.floor((mineDay-1)/7)+1
end

lunoschedule.getMarkingCol = function(mineDay) --return 1 to 7
	local nowRow = lunoschedule.getMarkingRow(mineDay) --return 1 to 4
	return mineDay - ((nowRow-1)*7)
end
lunoschedule.getNowMarkingRow = function() --return 1 to 4
	--math.ceil(arredonda para cima de 0.5), math.floor(arredonda para menor de 0.5)
	local mineDay = lunoschedule.getNowDay()
	return lunoschedule.getMarkingRow(mineDay) --return 1 to 4
end

lunoschedule.getNowMarkingCol = function() --return 1 to 7
	local mineDay = lunoschedule.getNowDay() --returne 1 to 28
	return lunoschedule.getMarkingCol(mineDay) --return 1 to 7
end

lunoschedule.getObjectID = function(month, day)
	return "lunoschedule:schedule_"..month.."_"..day
end

lunoschedule.getGroupsID = function(month, day)
	local groupsID = {
		not_in_creative_inventory = 1,
		--falling_node = 1, --Despenca no chão se mal empilhado.
		flammable = 3, --Pode ser usado como combustível.
		--snappy = 2, 
		--choppy = 2, 
		dig_immediate = 2, --Pode se retirado do chão com facilidade.
		oddly_breakable_by_hand = 2, --Oode ser retirado usando domente as mãos.
		schedule = (((month-1)*28)+day), 
		month = month,
		day = day,
	}
	if month == 1 and day == 1 then 
		groupsID.not_in_creative_inventory = nil
	end
	--local groupsID = (myMonth==1 and myDay==1)?{snappy=2,choppy=2,oddly_breakable_by_hand=2}:{snappy=2,choppy=2,oddly_breakable_by_hand=2,not_in_creative_inventory=1}
	return groupsID
end

lunoschedule.getTileID = function(month, day)
	local lang = lunoschedule.getLanguage()
	--local tileID = {"tex_background.pt.png^[combine:200x200:0,0=tex_schedule_inverno.png:0,0=tex_schedule_select.png"	},
	local tileID = "tex_face_front.png^[combine:200x200:0,0=tex_week."..lang..".png"
	if month~=nil and type(month)=="number" and month>=1 and month<=4 then
		tileID = tileID .. ":0,0=tex_season_"..month.."."..lang..".png"
	else
		--print("[ERRO CRITICO] Nao foi possivel imprimir o nome do month no calendario!")
		minetest.log("error", "["..string.upper(minetest.get_current_modname()).."] "..
			lunoschedule.translate("Could not print month name in calendar!")
		)
		return ""
	end
	--math.ceil(arredonda para cima de 0.5), math.floor(arredonda para menor de 0.5)
	--local row = math.ceil(day / 7)
	--local col = day - ((row-1)*7)
	local row = lunoschedule.getMarkingRow(day) --return 1 to 4
	local col = lunoschedule.getMarkingCol(day) --return 1 to 7
	
	tileID = tileID .. ":".. math.floor(((col-1)*19.7)+0)..",".. math.floor(((row-1)*16)+0).."=tex_marking.png"
	return tileID
end

lunoschedule.doUpdate = function(pos)
	local nowMes = lunoschedule.getNowMonth()
	local nowDia = lunoschedule.getNowDay()
	local newSchedule = lunoschedule.getObjectID(nowMes, nowDia)
	local nowNode = minetest.get_node(pos)
	minetest.set_node(pos, {
		name = newSchedule,
		param2 = nowNode.param2
	})
	minetest.get_meta(pos):set_string("infotext", 
		core.colorize("#00FF00", 
			lunoschedule.translate("SCHEDULE")
		)
		.."\n\t* "..lunoschedule.translate("Current Date: %s"):format(
			("%04d-%02d-%02d"):format((lunoschedule.getTotalYears()+1), nowMes, nowDia)
		)
	)
	
end

lunoschedule.doCheckUpdate = function(pos)
	local nowMes = lunoschedule.getNowMonth()
	local nowDia = lunoschedule.getNowDay()
	local newSchedule = lunoschedule.getObjectID(nowMes, nowDia)
	if newSchedule ~= minetest.get_node(pos).name then
		lunoschedule.doUpdate(pos)
		minetest.add_particlespawner({
			amount = 16*5,
			time = 5,
			minpos = {x=pos.x-0.5, y=pos.y-0.15, z=pos.z-0.50},
			maxpos = {x=pos.x+0.5, y=pos.y+0.15, z=pos.z-0.25},
			minvel = {x=-0.8,	y=-0.3,	z=-0.8},
			maxvel = {x=0.8,	y=0.3,	z=0.8},
			minacc = {x=0, y=0, z=0},
			maxacc = {x=0, y=0, z=0},
			minexptime = 0.5,
			maxexptime = 2, --defaul: 2
			minsize = 1,
			maxsize = 2, 			
			collisiondetection = false,
			vertical = true,
			texture = "tex_particle_yellow.png",
			--playername = "singleplayer",
		})
	end
end


