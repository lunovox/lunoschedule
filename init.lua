local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

dofile(modpath.."/api.lua")
dofile(modpath.."/translate.lua")
dofile(modpath.."/item_schedule.lua")
dofile(modpath.."/item_computing_app.lua")

--minetest.log('action','['..string.upper(modname)..'] Carregado!')
minetest.log("action", "["..string.upper(modname).."] Carregado!")
