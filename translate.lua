local ngettext

--[[
local S = minetest.get_translator('testmod')
minetest.register_craftitem('testmod:test', {
    description = S('I am a test object'),
    inventory_image = 'default_stick.png^[brighten'
})
--]]

if minetest.get_modpath("intllib") then
	if intllib.make_gettext_pair then
		-- New method using gettext.
		lunoschedule.translate, ngettext = intllib.make_gettext_pair()
	else
		-- Old method using text files.
		lunoschedule.translate = intllib.Getter()
	end
elseif minetest.get_translator ~= nil and minetest.get_current_modname ~= nil and minetest.get_modpath(minetest.get_current_modname()) then
	lunoschedule.translate = minetest.get_translator(minetest.get_current_modname())
else
	lunoschedule.translate = function(s) return s end
end

lunoschedule.getLanguage = function()
	if lunoschedule.language == nil or type(lunoschedule.language)~="string" or lunoschedule.language == "" then
		local LANG = minetest.settings:get("language")
		if not (LANG and (LANG ~= "")) then LANG = os.getenv("LANG") end
		if not (LANG and (LANG ~= "")) then LANG = "en" end
		lunoschedule.language = LANG
	end
	return lunoschedule.language
end