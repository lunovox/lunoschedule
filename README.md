![screenshot1]

# LUNOSCHEDULE

[Minetest Mod] Add a wall calendar, with seasons instead of months, and 28 days for each season.

**Recipe:**
![recipe]

**Dependencies:**
  * default → Minetest Game Included

**Optional Dependencies:**
  * [intllib] → Facilitates the translation of several other mods into your native language, or other languages.
  * [tradelands] → Protection of your lands and your objects like this calendar. 

**Licence:**
 * GNU AGPL: https://pt.wikipedia.org/wiki/GNU_Affero_General_Public_License

**Developers:**
 * Lunovox Heavenfinder: [email](mailto:lunovox@disroot.org), [social web](http:mastodon.social/@lunovox), [webchat](https://cloud.disroot.org/call/9aa2t7ib), [xmpp](xmpp:lunovox@disroot.org?join), [Mumble](mumble:mumble.disroot.org), [more contacts](https:libreplanet.org/wiki/User:Lunovox)

**Languages:**

 * [English] (Default)
 * [Português (Genérico)] (with [intllib] mod)
 * [Português Brasileiro] (with [intllib] mod)

Para criar sua propria tradução que o [locale/README.md]!

-----
[English]:https://gitlab.com/lunovox/lunoschedule/-/raw/master/locale/template.pot
[intllib]:https://github.com/minetest-mods/intllib
[locale/README.md]:https://gitlab.com/lunovox/lunoschedule/-/blob/master/locale/README.md
[Português (Genérico)]:https://gitlab.com/lunovox/lunoschedule/-/raw/master/locale/pt.po
[Português Brasileiro]:https://gitlab.com/lunovox/lunoschedule/-/raw/master/locale/pt_BR.po
[recipe]:https://gitlab.com/lunovox/lunoschedule/-/raw/master/textures/img_recipe.png
[screenshot1]:https://gitlab.com/lunovox/lunoschedule/-/raw/master/textures/img_calendar.png
[tradelands]:https://gitlab.com/lunovox/tradelands